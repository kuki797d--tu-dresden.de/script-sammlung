#!/usr/bin/python3

import json
import argparse
import warnings
from os import remove
from os.path import isfile, exists, basename
from atexit import register
from sys import exit, stdout
import datetime
from socket import inet_aton
import struct
from pyVim.connect import *
from pyVmomi import vmodl, vim

# This script checks all VMs with the custom field "SERVICE_CLASS"=="PaaS" and tries to read out the guest-os from the vm-object as well as the custom field "PRIMARY_IP_ADDRESS".
# If everything is available the script will check the guest-os-string for any "identifiers" under  any "os_configuration" as given in the config.
# The ip-addresses for each "os_configuration" for each "vcenter_configuration" are temporarily written to a file under /tmp.
# After everything is done, the temporary files of each "os_configuration" are combined, sorted and written to the "outputfile" given in the config.
# (so that the same "os_configuration" from different "vcenter_configuration" are combined as well)


def arg_parser():
    # Just the arguments parser
    my_parser = argparse.ArgumentParser(description="Metric script")

    my_parser.add_argument(
        "--config",
        action="store",
        type=str,
        help="Path to a config file.",
    )

    args = my_parser.parse_args()

    if args.config is not None:
        if not isfile(args.config):
            print("The path specified from -C / --config does not exist")
            exit()
    else:
        print("config file is required. -C / --config")
        exit()

    return args


def sort_ips(ips):
    return sorted(ips, key=lambda ip: struct.unpack("!L", inet_aton(ip))[0])


def main():
    # argparser
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    s = ssl.SSLContext()
    s.verify_mode = ssl.CERT_NONE
    args = arg_parser()

    with open(args.config) as json_file:
        loaded = json.load(json_file)
        vcenters = loaded["vcenter_configuration"]
        WARNINGS = loaded["warnings"]

    for vcenter in vcenters:
        # connect
        print("Starting with VCenter {}".format(vcenter["hostname"]))

        si = SmartConnect(
            host=vcenter["hostname"],
            user=vcenter["username"],
            pwd=vcenter["password"],
            sslContext=s,
            port=443,
        )
        register(Disconnect, si)
        content = si.RetrieveContent()

        # the custom attributes are mapped to a key (a unique number)
        # we first have to get this id trough customFieldsmanager
        SERVICE_CLASS_ID = None
        PRIMARY_IP_ADDRESS_ID = None

        cfm = content.customFieldsManager
        for some_field in cfm.field:
            if some_field.name == "SERVICE_CLASS":
                SERVICE_CLASS_ID = some_field.key

            elif some_field.name == "PRIMARY_IP_ADDRESS":
                PRIMARY_IP_ADDRESS_ID = some_field.key

        if not SERVICE_CLASS_ID or not PRIMARY_IP_ADDRESS_ID:
            print(
                "Couldn't find the key to SERVICE_CLASS or PRIMARY_IP_ADDRESS custom field."
            )
            exit(1)

        # get all avaialble vm objects
        container = content.rootFolder
        viewType = [vim.VirtualMachine]
        recursive = True
        containerView = content.viewManager.CreateContainerView(
            container, viewType, recursive
        )
        vms = containerView.view

        # those are the configurations for the operating systems
        os_check_map = vcenter["os_configuration"]
        # create some empty array, in which the ips are stored
        for os in os_check_map:
            os["ips"] = []

        for vm in vms:
            if vm.guest and vm.config and vm.config.extraConfig:

                # check if PaaS is set
                if any(
                    cfg_opt.key == SERVICE_CLASS_ID and cfg_opt.value == "PaaS"
                    for cfg_opt in vm.customValue
                ):
                    # check if vm.guest is accessible
                    if not vm.guest:
                        vm_guest_os = vm.config.guestFullName
                    else:
                        vm_guest_os = vm.guest.guestFullName

                    vm_ip_address = next(
                        (x for x in vm.customValue if x.key == PRIMARY_IP_ADDRESS_ID),
                        None,
                    )
                    vm_ip_address = (
                        vm_ip_address.value if vm_ip_address is not None else None
                    )

                    # check if guest os & ip address are accessible
                    if vm_guest_os is None or vm_ip_address is None:
                        if WARNINGS:
                            print(
                                "VM {} - can't read IP Address or GuestOS Name. Skipping.".format(
                                    vm.name
                                )
                            )
                        continue

                    # go over all os-configurations given under the os_configuration key in the config file
                    for os in os_check_map:
                        # check if any of the identifiers given in the config file match anything in the guestFullName
                        if (
                            any(
                                identifier in vm_guest_os
                                for identifier in os["identifiers"]
                            )
                            and vm_ip_address not in os["ips"]
                        ):
                            os["ips"].append(vm_ip_address)

        # write temporary files to /tmp for this vcenter
        for os in os_check_map:
            if exists("/tmp"):
                name = basename(os["outputfile"])
                os["temporary_outputfile"] = (
                    "/tmp/" + name + vcenter["hostname"] + ".tmp"
                )
            else:
                name = basename(os["outputfile"])
                os["temporary_outputfile"] = name + vcenter["hostname"] + ".tmp"

            # write the collected ip addresses of this vcenter to a temporary output file in /tmp
            with open(os["temporary_outputfile"], "w") as f:
                f.write("\n".join(os["ips"]) + "\n")

        print("Done with VCenter {}".format(vcenter["hostname"]))

    # map the temporary files to the output-file-path of the os
    # (key = outputpath, value= array of temporary files that will be written to the outputpath)
    file_map = {}
    for vcenter in vcenters:
        os_check_map = vcenter["os_configuration"]
        for os in os_check_map:
            if os["outputfile"] in file_map:
                file_map[os["outputfile"]].append(os["temporary_outputfile"])
            else:
                file_map[os["outputfile"]] = [os["temporary_outputfile"]]

    # write all temp files to their actual files
    for output_file in file_map:
        with open(output_file, "w") as write_file:
            all_ips = []
            # go over all temporary files belonging to the output_file of the current os
            # and add any ip address in those files to all_ips
            for read_file in file_map[output_file]:
                with open(read_file, "r") as infile:
                    temp_ips = infile.read().split()
                    for temp_ip in temp_ips:
                        all_ips.append(temp_ip)
                remove(read_file)

            # sort all_ips by ip address
            all_ips = sort_ips(all_ips)

            write_file.write("\n".join(all_ips) + "\n")


if __name__ == "__main__":
    main()
