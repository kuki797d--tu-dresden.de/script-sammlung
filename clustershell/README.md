# Clustershell Auto-Update Script

- **Setup**
    - ```clustershell_add_cfg``` to ```/home/jmp```
    - ```nagios/check_update_clustershell_ips``` to ```/usr/lib/nagios/plugins``` 
    - ```systemd/*``` to ```/etc/systemd/system``` and start/enable

    - */etc/nagios/nrpe.cfg*
        - ```command[clsh_update]=sudo -u jmp /usr/lib/nagios/plugins/check_update_clustershell_ips```

- **clustershell_add_cfg/clsh_get_cfg.py**
    - This script checks all VMs with the custom field "SERVICE_CLASS"=="PaaS" and tries to read out the guest-os from the vm-object as well as the custom field "PRIMARY_IP_ADDRESS". If everything is available the script will check the guest-os-string for any "identifiers" under  any "os_configuration" as given in the config. The ip-addresses for each "os_configuration" for each "vcenter_configuration" are temporarily written to a file under /tmp. After everything is done, the temporary files of each "os_configuration" are combined, sorted and written to the "outputfile" given in the config. (so that the same "os_configuration" from different "vcenter_configuration" are combined as well)
    - *Required Parameters*: --config
    - *Config*: See ```clustershell_add_cfg``` for examples